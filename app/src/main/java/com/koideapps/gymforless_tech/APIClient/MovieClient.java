package com.koideapps.gymforless_tech.APIClient;

import com.koideapps.gymforless_tech.Models.Movie;
import com.koideapps.gymforless_tech.Models.MovieModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieClient {

    @GET("movie/top_rated")
    Call<MovieModel> getTopMovies(@Query("api_key") String apiKey,
                                  @Query("page") int page);

    @GET("search/movie")
    Call<MovieModel> getMoviesWithSearch(@Query("api_key") String apiKey,
                                         @Query("query") String searchText,
                                         @Query("page") int page);

    @GET("movie/{id}")
    Call<Movie> getMovie(@Path("id") String movieId,
                         @Query("api_key") String apiKey);

}
