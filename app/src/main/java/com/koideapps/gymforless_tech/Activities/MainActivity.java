package com.koideapps.gymforless_tech.Activities;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.koideapps.gymforless_tech.APIClient.MovieClient;
import com.koideapps.gymforless_tech.Adapters.MovieItemAdapter;
import com.koideapps.gymforless_tech.Models.MovieModel;
import com.koideapps.gymforless_tech.R;
import com.koideapps.gymforless_tech.Utils.APPConstants;
import com.mancj.materialsearchbar.MaterialSearchBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements MaterialSearchBar.OnSearchActionListener  {

    private RecyclerView movieRecycler;
    private MaterialSearchBar searchBar;
    private MovieItemAdapter adapter;
    private ProgressDialog progressDialog;
    private MovieModel movieList;

    private String searchText;

    private int currentPage = 1;
    private int totalPage = 1;

    private MovieClient getAPIClient () {
        Retrofit.Builder builder =new Retrofit.Builder()
                .baseUrl(APPConstants.getApiUrl())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.build().create(MovieClient.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MovieItemAdapter(this);
        progressDialog = ProgressDialog.show(this, "Loading", "Wait", true);

        setupUI();
        setupRecycler();
        setupSearchBar();
        getMovieData(null);
    }

    private void setupUI() {
        movieRecycler = (RecyclerView) findViewById(R.id.movie_list);
        searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
    }

    private void setupSearchBar() {
        searchBar.setOnSearchActionListener(this);
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        movieRecycler.setLayoutManager(layoutManager);
        movieRecycler.setItemAnimator(new DefaultItemAnimator());
        movieRecycler.setAdapter(adapter);
        movieRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) movieRecycler.getLayoutManager();

                int totalItems = layoutManager.getItemCount();
                int visibleItems = layoutManager.getChildCount();
                int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();

                if (pastVisibleItems + visibleItems >= totalItems) {
                    if (currentPage < totalPage) {
                        currentPage++;
                        getMovieData(searchText);
                    }
                }
            }
        });
    }

    private void getMovieData(final CharSequence searchText) {
        Call<MovieModel> call;

        if (searchText == null) {
            call = getAPIClient().getTopMovies(APPConstants.getApiKey(), currentPage);
        } else {
            call = getAPIClient().getMoviesWithSearch(APPConstants.getApiKey(), searchText.toString(), currentPage);
        }

        call.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(Call<MovieModel> call, Response<MovieModel> response) {
                movieList = response.body();
                totalPage = movieList.getTotal_pages();
                progressDialog.dismiss();

                if (currentPage > 1) {
                    adapter.addMovies(movieList.results);
                } else {
                    adapter.startMovies(movieList.results);
                }
            }
            @Override
            public void onFailure(Call<MovieModel> call, Throwable t) {
                ProgressDialog.show(MainActivity.this, "Error", "Error just happened", true);
            }
        });
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {

    }

    @Override
    public void onSearchConfirmed(CharSequence finalText) {
        searchBar.clearFocus();
        if (finalText != null) {
            currentPage = 1;
            totalPage = 1;
            searchText = finalText.toString();
            getMovieData(finalText);
        }
    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

}
