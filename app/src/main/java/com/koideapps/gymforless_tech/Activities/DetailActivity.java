package com.koideapps.gymforless_tech.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.koideapps.gymforless_tech.APIClient.MovieClient;
import com.koideapps.gymforless_tech.Models.Genre;
import com.koideapps.gymforless_tech.Models.Movie;
import com.koideapps.gymforless_tech.R;
import com.koideapps.gymforless_tech.Utils.APPConstants;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailActivity extends AppCompatActivity {

    private TextView detailTitle, detailRating, detailYear, detailGenre, detailBudget, detailOverview;
    private ImageView detailImage;

    private ProgressDialog progressDialog;

    private Movie movieDetail;

    private MovieClient getAPIClient () {
        Retrofit.Builder builder =new Retrofit.Builder()
                .baseUrl(APPConstants.getApiUrl())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.build().create(MovieClient.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        movieDetail = (Movie) getIntent().getSerializableExtra("movieClass");
        progressDialog = ProgressDialog.show(this, "Loading", "Wait", true);

        setupUI();
        getDetailData();
    }

    private void setupUI() {
        detailTitle = (TextView) findViewById(R.id.detailTitle);
        detailImage = (ImageView) findViewById(R.id.detailImage);

        detailRating = (TextView) findViewById(R.id.detailRating);
        detailYear = (TextView) findViewById(R.id.detailYear);
        detailGenre = (TextView) findViewById(R.id.detailGenre);
        detailBudget = (TextView) findViewById(R.id.detailBudget);
        detailOverview = (TextView) findViewById(R.id.detailOverview);
    }

    private void fillUIWithMovie(Movie movieDetail) {
        detailTitle.setText(movieDetail.getTitle());
        Picasso.with(DetailActivity.this).load(APPConstants.getImgOriginalUrl()+movieDetail.getBackdrop_path()).into(detailImage);
        detailRating.setText(movieDetail.getVote_average().toString() + " / 10");
        detailOverview.setText(movieDetail.getOverview());
        detailBudget.setText(movieDetail.getRevenue());
        detailYear.setText(movieDetail.getRelease_date());

        Genre genre = movieDetail.getGenres().get(0);
        detailGenre.setText(genre.getName());
    }

    private void getDetailData() {
        Call<Movie> call = getAPIClient().getMovie(movieDetail.getId(), APPConstants.getApiKey());
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                Movie movie = response.body();
                fillUIWithMovie(movie);
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                progressDialog.dismiss();
                ProgressDialog.show(DetailActivity.this, "Error", "Error just happened", true);
            }
        });
    }
}
