package com.koideapps.gymforless_tech.Models;

import java.io.Serializable;
import java.util.List;

public class Movie implements Serializable {
    private String title, backdrop_path, release_date, overview,id, revenue;
    private Number vote_average;
    private List<Genre> genres;

    public List<Genre> getGenres() { return genres; }
    public String getId() { return id; }
    public String getRevenue() { return revenue; }
    public String getOverview() { return overview; }
    public Number getVote_average() { return vote_average; }
    public String getRelease_date() { return release_date; }
    public String getBackdrop_path() { return backdrop_path; }
    public String getTitle() {
        return title;
    }
}
