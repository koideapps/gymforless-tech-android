package com.koideapps.gymforless_tech.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieModel {

    private int total_results, total_pages;

    @SerializedName("results")
    public List<Movie> results;
    public int getTotal_pages() { return total_pages; }
    public int getTotal_results() { return total_results; }
}
