package com.koideapps.gymforless_tech.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koideapps.gymforless_tech.Activities.DetailActivity;
import com.koideapps.gymforless_tech.Models.Movie;
import com.koideapps.gymforless_tech.R;
import com.koideapps.gymforless_tech.Utils.APPConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieItemAdapter extends RecyclerView.Adapter<MovieItemAdapter.MyViewHolder> {

    private List<Movie> movieList = new ArrayList<>();
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView titleView, ratingView, yearView;
        private ImageView imageCardView;

        public MyViewHolder(View view) {
            super(view);

            cardView = (CardView) view.findViewById(R.id.cardView);
            titleView = (TextView) view.findViewById(R.id.textCardView);
            ratingView = (TextView) view.findViewById(R.id.ratingCardView);
            yearView = (TextView) view.findViewById(R.id.yearCardView);
            imageCardView = (ImageView) view.findViewById(R.id.imageCardView);
        }
    }

    public MovieItemAdapter(Context context) {
        this.context = context;
        movieList = new ArrayList<>();
    }

    public void addMovies(List<Movie> movieModel) {
        this.movieList.addAll(movieModel);
        notifyDataSetChanged();
    }

    public void startMovies(List<Movie> movieModel) {
        this.movieList.clear();
        this.movieList = movieModel;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MovieItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.activity_recycler_item, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieItemAdapter.MyViewHolder myViewHolder, int i) {
        final Movie movie = movieList.get(i);

        myViewHolder.titleView.setText(movie.getTitle());
        myViewHolder.ratingView.setText(movie.getVote_average().toString());
        myViewHolder.yearView.setText(movie.getRelease_date());
        Picasso.with(myViewHolder.imageCardView.getContext())
                .load(APPConstants.getImgOriginalUrl()+movie.getBackdrop_path())
                .into(myViewHolder.imageCardView);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("movieClass", movie);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
