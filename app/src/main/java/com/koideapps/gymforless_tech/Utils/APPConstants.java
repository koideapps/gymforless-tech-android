package com.koideapps.gymforless_tech.Utils;

public class APPConstants {
    public static final String API_URL = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "3a5415d988cec0a6e258c736356d2997";
    public static final String IMG_ORIGINAL_URL = "https://image.tmdb.org/t/p/original/";

    public static String getApiUrl() {
        return API_URL;
    }
    public static String getApiKey() {
        return API_KEY;
    }
    public static String getImgOriginalUrl() { return IMG_ORIGINAL_URL; }
}
